package org.gproman.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.gproman.calc.WingSplitInterpolator;
import org.gproman.db.DataService;

import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.FormLayout;

public class CalculoTemperaturaDesgastePanel extends UIPluginBase {

    private static final long serialVersionUID = 210232127277861273L;

    private JSpinner TemperaturaStint1 = new JSpinner(new SpinnerNumberModel(30.0, 1.0, 80.0, 0.1));
    private JSpinner TemperaturaStint2 = new JSpinner(new SpinnerNumberModel(30.0, 1.0, 80.0, 0.1));
    private JSpinner KmTotal1 = new JSpinner(new SpinnerNumberModel(100.0, 1.0, 400.0, 0.1));
    private JSpinner KmTotal2 = new JSpinner(new SpinnerNumberModel(100.0, 1.0, 400.0, 0.1));
    private JLabel idealWS = new JLabel();

    public CalculoTemperaturaDesgastePanel(GPROManFrame gproManFrame,
            DataService dataService) {
        super(gproManFrame,
                dataService);
        setLayout(new BorderLayout());

        // Building the second column
        FormLayout layout = new FormLayout("120dlu, 5dlu, 50dlu", "20dlu, 20dlu, 20dlu, 20dlu,20dlu, 20dlu");
        DefaultFormBuilder builder = new DefaultFormBuilder(layout);
        builder.border(Borders.DIALOG);

        idealWS.setText("0");
        idealWS.setHorizontalAlignment(SwingConstants.RIGHT);
        Font bold = idealWS.getFont().deriveFont(Font.BOLD);

//        JLabel dif = new JLabel("Diferença");
//        JLabel time = new JLabel("Tempo");
//        builder.append(dif, time);
//        dif.setFont(bold);
//        time.setFont(bold);
//        builder.nextLine();

        builder.appendSeparator("Conversão de desgaste de pneu aproximado:");
        builder.nextLine();
        
        JLabel lbl = new JLabel("Temperatura stint 1: ");
        builder.append(lbl, 1);
        lbl.setHorizontalAlignment(SwingConstants.RIGHT);
        lbl.setFont(bold);
        builder.append(TemperaturaStint1, 1);
        TemperaturaStint1.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                updateFields();
            }
        });

        builder.nextLine();

        lbl = new JLabel("KM Total stint 1: ");
        builder.append(lbl, 1);
        lbl.setHorizontalAlignment(SwingConstants.RIGHT);
        lbl.setFont(bold);
        builder.append(KmTotal1, 1);
        KmTotal1.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                updateFields();
            }
        });
        
        builder.nextLine();

        lbl = new JLabel("Temperatura stint 2: ");
        builder.append(lbl, 1);
        lbl.setHorizontalAlignment(SwingConstants.RIGHT);
        lbl.setFont(bold);
        builder.append(TemperaturaStint2, 1);
        TemperaturaStint2.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                updateFields();
            }
        });
        
        builder.nextLine();
        
        lbl = new JLabel("KM Total stint 2: ");
        builder.append(lbl, 1);
        lbl.setHorizontalAlignment(SwingConstants.RIGHT);
        lbl.setFont(bold);
        builder.append(KmTotal2, 1);
        KmTotal2.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                updateFields();
            }
        });

        builder.appendSeparator();
        idealWS.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 10));
        idealWS.setBackground(Color.BLUE);
        idealWS.setForeground(Color.WHITE);
        idealWS.setFont(bold);
        idealWS.setOpaque(true);
        JLabel lbl2 = builder.append("Quantidade de KM para cada grau: ", idealWS);
        lbl2.setFont(bold);
        lbl2.setHorizontalAlignment(SwingConstants.RIGHT);

        JPanel panel = builder.getPanel();
        add(panel, BorderLayout.CENTER);
    }

    private void updateFields() {
        double temp1 = ((SpinnerNumberModel) TemperaturaStint1.getModel()).getNumber().doubleValue();
        double temp2 = ((SpinnerNumberModel) TemperaturaStint2.getModel()).getNumber().doubleValue();
        double km1 = ((SpinnerNumberModel) KmTotal1.getModel()).getNumber().doubleValue();
        double km2 = ((SpinnerNumberModel) KmTotal2.getModel()).getNumber().doubleValue();
        
        double result1 = temp2 - temp1;
        double result2 = km1 - km2;

        double result3 = result2 / result1;
        
        idealWS.setText(String.valueOf(String.format("%2.2f ", result3)));
    }

    @Override
    public void update() {
        setDirty(false);
    }

    @Override
    public String getTitle() {
        return "Conversão desgaste";
    }

    @Override
    public ImageIcon getIcon() {
        return UIUtils.createImageIcon("/icons/tyres_32.png");
    }

    @Override
    public ImageIcon getSmallIcon() {
        return UIUtils.createImageIcon("/icons/tyres_16.png");
    }

    @Override
    public String getDescription() {
        return "Calculo de conversão de desgaste de pneus";
    }

    @Override
    public Category getCategory() {
        return Category.CALC;
    }

    @Override
    public int getMnemonic() {
        return KeyEvent.VK_A;
    }

    @Override
    public boolean isEnabledByDefault() {
        return true;
    }
}
